from gameParser.items import GameItem
import scrapy
import datetime


class Spider(scrapy.Spider):
    name = ''
    start_urls = ['']

    def parse(self, response):
        now = datetime.datetime.now()
        for post in response.css(''):
            Item = GameItem()

            Item['title'] = post.css('::text').extract_first()
            Item['author'] = post.css('::text').extract()
            Item['tags'] = post.css('::text').extract()
            Item['date'] = post.css('::text').extract_first()
            Item['scan_date'] = now.strftime("%d-%m-%Y %H:%M")
            Item['link'] = response.urljoin(post.css('::attr(href)').extract_first())

            request = scrapy.Request(Item['link'],callback=self.post_body)
            request.meta['Item'] = Item
            yield request

    #    next_page_url = response.css('::attr(href)').extract_first()
    #    if next_page_url:
    #        yield scrapy.Request(response.urljoin(next_page_url))

    def post_body(self, response):
        Item = response.meta['Item']

        Item['body'] = response.css("::text").extract()
        yield Item