from gameParser.items import GameItem
import scrapy
from random import choice
import logging


PRICES = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1200, 1400, 1500, 1600, 1800, 1900, 2000, 2500, 3000, 3200]
CATEGORIES = {'MMOG': 0, 'Ужасы': 1, 'Файтинг': 2, 'Стратегия': 3, 'Спорт': 4, 'Ролевая игра': 5, 'Приключение': 6,
              'Платформер': 7, 'Боевик': 8, 'Аркада': 9, 'Симулятор': 10, 'Логика': 11, 'Тактика': 12,
              'Симулятор жизни': 13, 'Космос': 14, 'Автомобили': 14}
CODES = ['VHHWK-69Y6M-WM8YD-MB8TR-K86FB', 'D36RK-QDFFD-BTWWY-BT7KK-43MGM', 'B79GC-DQF9M-RWB2D-C2BRT-2GV38',
         'P4XK3-TGQ3P-F9JB2-GC6XQ-VXTMW', 'THMPV-77D6F-94376-8HGKG-VRDRQ', 'QVQ43-P6FHD-K2G7C-WV8CT-H4TCM',
         'DP7CM-PD6MC-6BKXT-M8JJ6-RPXGJ', 'DDTPV-TXMX7-BBGJ9-WGY8K-B9GHM', 'VRBJ3-VMKG2-33WY4-2Q683-W7KMY',
         'K4HVD-Q9TJ9-6CRX9-C9G68-RQ2D3', 'QDQQ4-Q9WKB-GKBDJ-79DP2-YM8Y4', 'DTXM2-YVDH9-JHYV2-MPCJH-CCRFH',
         'HGBRM-RBK3V-M9FXV-YCXDK-V38J4', 'VMGGK-72FPD-2PHRP-3HV4R-FYJQJ', 'DQYJW-K4HGQ-DKW3T-673GY-PT8F8',
         'V2JCW-VRT4Y-YC2KJ-X9VC-T90CD', 'V9FKD-BY6B6-9JTJH-YCT3H-CJDX7', 'WMC8Q-RFPVP-W3JT6-W4QCP-XB9BF',
         'Q99PM-2QHFW-FQQMM-WBHK7-XMW4V', 'RBDC9-VTRC8-D7972-J97JY-PRVMG', 'WY6PG-M2YPT-KGT4H-CPY6T-GRDCY',
         'H6TWQ-TQQM8-HXJYG-D69F7-R84VM']


class Spider(scrapy.Spider):
    name = 'igromania'

    NEXT_PAGE_URL = 'https://www.igromania.ru/games/all/all/{}/'

    # create logger with 'spam_application'
    logger = logging.getLogger('categories_application')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('new_categories.log')
    fh.setLevel(logging.DEBUG)
    logger.addHandler(fh)

    def start_requests(self):
        for page in range(1, 3146):
            yield scrapy.Request(self.NEXT_PAGE_URL.format(page), callback=self.parse)

    def parse(self, response):
        for game in response.css('.gamebase_box'):
            Item = GameItem()

            Item['name'] = game.css('.release_name::text').get()

            Item['text'] = game.css('.release_data::text').get()
            Item['imageURL'] = game.css('.info_block_picbox img::attr(src)').get()

            Item['price'] = choice(PRICES)
            Item['quantity'] = 10
            Item['valide'] = 'true'
            Item['code'] = choice(CODES)
            Item['client_id'] = 0

            category = game.css('.genre a::text').get()
            try:
                Item['category_id'] = CATEGORIES[category]
            except KeyError:
                try:
                    category2 = game.css('.genre a::text').getall()[1]
                    Item['category_id'] = CATEGORIES[category2]
                except (KeyError, IndexError):
                    self.logger.debug(category)
            yield Item
