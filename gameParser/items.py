# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class GameItem(scrapy.Item):
    name = scrapy.Field()
    category_id = scrapy.Field()
    text = scrapy.Field()
    imageURL = scrapy.Field()
    price = scrapy.Field()
    quantity = scrapy.Field()
    valide = scrapy.Field()
    code = scrapy.Field()
    client_id = scrapy.Field()
